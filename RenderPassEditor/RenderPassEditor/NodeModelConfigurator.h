#ifndef __NODECONFIGURATOR_H__
#define __NODECONFIGURATOR_H__

# include "Application.h"
# include <imgui_node_editor.h>
# define IMGUI_DEFINE_MATH_OPERATORS
# include <imgui.h>
# include <imgui_internal.h>
# include <algorithm> 
# include <string>
# include "NodeModelHandler.h"

// Displays Warning PopUp
void invalid_node () ;

// Copy Datas and create the node model into our node models vector
void create_node () ;
void create_node (NodeModelsHandler & nodes) ;

// Displays configuration fields according to sliders values  
void display_parameters () ;

// Removes last node from our node models vector ...
void delete_last_node (NodeModelsHandler & nodes) ;

// Removes the node of id nodeId from our node models vector
void delete_node (NodeModelsHandler & nodes, ed::NodeId nodeId) ;

// Displays node creation ImGui interface
void NodeCreator (NodeModelsHandler & nodes) ;

#endif