#ifndef __NODEMODELHANDLER_H__
#define __NODEMODELHANDLER_H__

# include <vector>
# include <iostream>

namespace ed = ax::NodeEditor ;

class NodeModel{

	public :

		ed::NodeId id ;						// node ID
		std::string name ;			 		// node name
		std::vector <std::string> inputs ; 	// inputs name
		std::vector <std::string> outputs ; // outputs name
		int nbOutput, nbInput ;				// number of input / output

		// Inline NodeModel constructor
		inline NodeModel (ed::NodeId idN, char node_name [20], std::vector <std::string> in, std::vector <std::string> out, int& nbIn, int& nbOut) 
			: id(idN) , name (node_name) , nbOutput (nbOut) , nbInput (nbIn) , inputs (in) , outputs (out) {}
} ;

class NodeModelsHandler{

		// Our vector of node models
		std::vector <NodeModel> nodes ;
	
	public :

		// vector containing selected node ids
		// Used in RenderPassEditor.cpp
		std::vector <ed::NodeId> selectedNodes; 

		// Adds a node model into our node models vector
		inline void add (NodeModel& toAdd) { nodes.push_back (toAdd) ; }

		// Removes the last node from our node models vector
		inline void pop () { if ( nodes.size () > 0 ) nodes.erase (nodes.begin () + nodes.size () - 1) ; }

		// Returns the last node of our node models vector
		inline NodeModel& nodes_back () { return nodes.back () ; }

		// Returns our node models vector
		inline std::vector<NodeModel>& getNodes () { return nodes ; }

		// Removes the node of id nodeId from our node models vector
		inline void delete_node ( ed::NodeId nodeId ) { 
			int cur = 0 ; 
			for (NodeModel node : nodes) {
				if (nodeId == node.id) {
					nodes.erase (nodes.begin () ++ ) ;
					break ;
				}
			}
		}

		// Returns the size of our node models vector
		inline size_t size() { return nodes.size () ; }

		// Returns by ref the node model at index idx
	 	inline NodeModel& operator[] (std::size_t idx) { return nodes [idx] ; }

	 	// Returns by const ref the node model at index idx
		inline const NodeModel& operator[] (std::size_t idx) const { return nodes [idx] ; }
} ;

#endif